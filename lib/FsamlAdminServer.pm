package FsamlAdminServer;
use Mojo::Base 'Mojolicious', -signatures;

# Registers singleton resultsets services
sub load_schemas($app, $package_info) {
  my %package_info = %{ $package_info };
  my $namespace = $package_info{namespace};
  my $credentials = $package_info{credentials};

  foreach my $key (grep { $_ ne "namespace" && $_ ne "credentials" } keys %package_info) {
    my $schema_name = $package_info{$key}->{schema} || "Schema";
    my $full_schema_name = $namespace . '::' . ucfirst $key . '::' . $schema_name;
    my $resultsets = $package_info{$key}->{resultsets};
    eval "use $full_schema_name";
    my $schema = $full_schema_name->connect(@{ $credentials },
                                            { RaiseError => 1,
                                              RaiseWarn => 1,
                                              PrintError => 0,
                                              PrintWarn => 0});
    foreach my $resultset (@{ $resultsets }) {
      $app->helper((lc $resultset . 's') => sub {
        state $var = $schema->resultset($resultset);
      })
    }
  }
}

# Registers 'scoped' lifetime services
sub load_scoped_service($app, $services_info) {
  my %services = %{ $services_info };
  my $namespace = $services{namespace};
  my $channel_container = $namespace . '::Channel::ChannelContainer';

  foreach my $service (grep { $_ ne "namespace" } keys %services) {
    $app->helper($service => sub {
                   my $caller = shift;
                   eval "use $channel_container";
                   my $service_class =
                     $namespace . '::' . $services{$service}->{service_class};
                   state $cache =
                     $channel_container
                     ->new(group =>
                           $namespace .  '::' . $services{$service}->{group}
                           || $namespace . '::ChannelGroup::BasicChannelGroup',
                           service_class => sub ($controller) {
                             eval "use $service_class";
                             $service_class->new(tx => $controller->tx,
                                                 @{ $services{$service}->{args}->($app) })
                            });
                   return $cache->proxy($caller);
                 })
  }
}

# This method will run once at server start
sub startup ($self) {

  # Load configuration from config file
  my $config = $self->plugin('NotYAMLConfig');

  # Configure the application
  $self->secrets($config->{secrets});
  # Load database schemas
  $self->load_schemas({
                       namespace => 'FsamlAdminServer::Model',
                       credentials => [ 'dbi:mysql:fadmin', 'fsaml_admin', 'fsamladmin' ],
                       billing => {
                                   resultsets => [ 'Patient',
                                                   'Consultation',
                                                   'Payment',
                                                   'PaymentStatus' ]
                                  }
                      });

  # Load application services
  $self
    ->load_scoped_service({
                           namespace => 'FsamlAdminServer::Service',
                           workers => {
                                                service_class
                                                => qw\StreamWorker::RecurringStreamWorker\,
                                                group
                                                => qw\ChannelGroup::StreamWorkerGroup\,
                                                args => sub { [] }
                                               }
                          });
  # Add CORS Headers
  $self->hook(after_dispatch => sub {
                my $c = shift;
                $c->res->headers->header('Access-Control-Allow-Methods' =>
                                     'GET, OPTIONS, PUT, POST, DELETE');
                $c->res->headers->header('Access-Control-Allow-Headers' =>
                                         'Content-Type' =>
                                         '*');
                $c->res->headers->header('Access-Control-Allow-Origin' => '*');
              });
  # Router
  my $r = $self->routes;
  $r
    ->options('/*not_found'
              => { 'not_found' => '' }
              =>
              sub($c) {
                $c->render(text => 'Success!')
              });
  # API Routes
  my $api = $r->under("/api")->to(cb => sub {
    return 1
  });
  # Patient Routes
  my $patient = $api->any("/patient")->to(controller => "patient_controller");
  $patient->websocket("/stream")->to(action => "patient_stream");
  $patient->post("/create")->to(action => "create_patient");
  $patient->put("/update")->to(action => "update_patient");
  $patient->get("/history/:patient_cid")->to(action => "patient_history");
  # Consultation Routes
  my $consultation = $api->any("/consultation")->to(controller => 'consultation_controller');
  $consultation->websocket("/stream")->to(action => "consultation_stream");
  $consultation->post("/create")->to(action => 'create_consultation');
  $consultation->post("/update")->to(action => 'update_consultation');
  $consultation->get("/patient_information/:id")->to(action => 'patient_info');
  $consultation->get("/services/:id")->to(action => 'services');
}

1;

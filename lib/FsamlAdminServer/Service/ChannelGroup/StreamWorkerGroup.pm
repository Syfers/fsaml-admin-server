package FsamlAdminServer::Service::ChannelGroup::StreamWorkerGroup;
use FindBin qw($Bin);
use lib "$Bin/../../../../lib";
use Mojo::Base 'FsamlAdminServer::Service::ChannelGroup::BasicChannelGroup', -signatures;

sub refresh($self) {
  $self->send(json => { msg => "REFRESH" });
  $self->send(cb => 'registered');
}

1

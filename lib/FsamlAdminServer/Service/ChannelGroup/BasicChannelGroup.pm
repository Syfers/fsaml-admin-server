package FsamlAdminServer::Service::ChannelGroup::BasicChannelGroup;
use Mojo::Base -base, -signatures;

has 'topic' => '';
has 'members' => sub { [] };

sub send($self, %options) {
  my $destiny = $options{to} || "*";
  foreach my $member (@{ $self->members }) {
    $member->send(%options);
  }
}

1

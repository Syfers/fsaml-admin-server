package FsamlAdminServer::Service::StreamWorker::RecurringStreamWorker;
use Mojo::IOLoop;
use Mojo::Base -base, -signatures;
has 'active_reference' => undef;
has 'worker_cb' => undef;
has 'worker_timeout' => undef;
has 'query' => undef;
has 'params' => undef;
has 'tx' => undef;
has 'ioloop' => sub { Mojo::IOLoop->singleton };
# Channel Container props
has 'prefix' => undef;
has 'id' => sub ($self) {
  return $self->tx->req->request_id
};

sub cancel($self) {
  $self->ioloop->remove($self->active_reference);
  return $self;
}

sub send($self, %options) {
  my $method = $options{cb};
  if ($method) {
    return
      $self
      ->worker_cb($options{cb})
      ->worker_timeout($options{timeout})
      ->params($options{params})
      ->query($options{query})
      ->execute_scheduled if $options{cb} ne 'registered';
    return
      $self
      ->execute_scheduled;
  } else {
    delete $options{cb};
    delete $options{args};
    delete $options{query};
    delete $options{params};
    delete $options{timeout};
    $self->tx->send(\%options);
    return $self;
  }
}

sub execute_scheduled($self) {
  if ($self->tx && $self->query) {
    $self->query->reset;
    return
      $self
      ->active_reference(
                         $self->ioloop->recurring($self->worker_timeout =>
                                                  sub {
                                                    $self->execute_worker(@{ $self->params });
                                                  })
                        )
  }
}

sub execute_worker($self, @args) {
  $self->worker_cb->($self, @args);
}

1

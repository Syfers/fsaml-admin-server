package FsamlAdminServer::Service::Channel::ChannelContainerProxy;
use Mojo::Base -base, -signatures;

has 'container' => undef;
has 'controller' => undef;

sub register($self, $tx_object) {
  $tx_object->prefix(ref $self->controller) unless $tx_object->prefix;
  return $self->container->register($tx_object);
}

sub prefix($self, $prefix) {
  return $self->container->prefix($prefix);
}

sub unregister($self, $tx_obj) {
  $tx_obj->prefix(ref $self->controller) unless $tx_obj->prefix;
  return $self->container->unregister($tx_obj);
}

sub proxy($self, $c) {
  return $self;
}

sub get($self, $tx_obj) {
  $tx_obj->prefix(ref $self->controller) unless $tx_obj->prefix;
  return $self->container->get($tx_obj);
}

1

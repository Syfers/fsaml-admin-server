package FsamlAdminServer::Service::Channel::ChannelContainer;
use FindBin qw($Bin);
use lib "$Bin/../../../../lib";
use FsamlAdminServer::Service::Channel::ChannelContainerProxy;
use Mojo::Base -base, -signatures;

has 'tx_table' => sub { {  } };
has 'group' => undef;
has 'service_class' => undef;

sub register($self, $tx_object) {
  my $tid = $tx_object->id;
  my $prefix = $tx_object->prefix;
  $self->tx_table->{"$prefix:$tid"} ||= $tx_object;
  return $self;
}

sub get($self, $tx_object) {
  my $tid = $tx_object->id;
  my $prefix = $tx_object->prefix;
  return $self->tx_table->{"$prefix:$tid"};
}

sub prefix($self, $topic) {
  my $group = $self->group;
  eval "use $group";
  my $regex = qr($topic);
  my @members =
    map { $self->tx_table->{$_} }
    grep { $_ =~  $regex}
    keys %{ $self->tx_table };
  return $group->new(topic => $topic, members => \@members);
}

sub unregister($self, $tx_obj) {
  my $id = $tx_obj->id;
  my $prefix = $tx_obj->prefix;
  delete %{ $self->tx_table }{"$prefix:$id"};
  return $self;
}

sub proxy($self, $c) {
  return
    FsamlAdminServer::Service::Channel::ChannelContainerProxy
      ->with_roles('FsamlAdminServer::Role::ChannelControllerProxy')
      ->new(container => $self, controller => $c);
}

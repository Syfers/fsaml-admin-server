package FsamlAdminServer::Controller::PatientController;
use Mojo::JSON qw(decode_json);
use Encode;
use Try::Tiny;
use Mojo::IOLoop;
use Role::Tiny::With;
use Mojo::Base 'Mojolicious::Controller', -signatures;
with 'FsamlAdminServer::Role::StreamRole';

sub stream_rs($self) {
  $self->patients
}

sub patient_stream($self) {
  $self->stream_handshake;
}

sub create_patient($self) {
  my $patient = decode_json $self->req->body;
  my $patients = $self->patients;
  try {
    $patients->create($patient);
    $self->restream;
    $self->render(json => { msg => 'Message Created' });
  } catch {
    $self->res->message(sprintf 'El número de cedula %s ya está registrado', $patient->{cid})
      if $_ =~ m(Duplicate entry);
    $self->rendered(500);
  }
}

sub update_patient($self) {
  my $payload = decode_json $self->req->body;
  try {
    $self
      ->patients
      ->find({ cid => $payload->{previous_cid} })
      ->update($payload->{body});
    $self->restream;
    $self->render(json => "Record updated!");
  } catch {
    $self->res->message(sprintf 'El número de cedula %s ya está registrado',
                        $payload->{body}->{cid}) if $_ =~ m(Duplicate entry);
    $self->rendered(500);
  }
}

sub patient_history($self) {
  my $cid = $self->stash('patient_cid');
  my @patient_consultations =
    $self
    ->patients
    ->find({ cid => $cid })
    ->consultations
    ->search(undef, {
                     order_by => { -desc => 'consultation_date' }
                    });

  sub history_of ($consultation) {
    my $_ = $consultation;
    return { status => 'cancelled', date => $_->consultation_date } if $_->status == 1;
    return { status => 'in-progress', date => $_->consultation_date } if $_->status == 2;
    return { status => 'completed', date => $_->consultation_date } if $_->status == 3;
  }

  my @histories =
    map { history_of $_ } @patient_consultations;
  $self->render(json => \@histories)
}

1

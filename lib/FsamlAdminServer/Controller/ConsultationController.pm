package FsamlAdminServer::Controller::ConsultationController;
use Role::Tiny::With;
use Encode;
use Try::Tiny;
use Mojo::JSON qw(decode_json encode_json);
use Mojo::Base 'Mojolicious::Controller', -signatures;
with 'FsamlAdminServer::Role::StreamRole';

sub as_utf8(%hash) {
  return map { $_=> decode_utf8 $hash{$_} }
    grep { $_ ne "patient_id" && $_ ne "id" }
    keys %hash
}

sub stream_rs($self) {
  return
    $self
    ->consultations
    ->search(undef, { prefetch => 'patient' })
}

sub consultation_stream($self) {
  $self->stream_handshake
}

sub create_consultation($self) {
  my $consultation = decode_json $self->req->body;
  my $patient_cid = $consultation->{patient_id};
  try {
    my $patient =  $self->patients->find({ cid => $patient_cid });
    die sprintf "Paciente con cédula %d no existe", $patient_cid unless $patient;
    $patient
      ->create_related(consultations =>
                       {
                        map { $_=> $consultation->{$_} }
                        grep { $_ ne "patient_id" && $_ ne "id" }
                        keys %$consultation
                       });
    $self->restream;
    $self->render(json => { msg => "Consultation created" })
  } catch {
    warn $_;
    $self->res->message('Error creando consulta') if length $_ >= 50;
    $self->res->message($_) if length $_ < 50;
    $self->rendered(500);
  }
}

sub update_consultation($self) {
  my $req_body = decode_json $self->req->body;
  say $self->req->body;
  my $prev_consultation_id = $req_body->{pid};
  my $new_consultation = $req_body->{consultation};

  try {
    $self
      ->consultations
      ->find({ id => $prev_consultation_id })
      ->update($new_consultation);
    $self->restream;
    $self->render(json => { msg => "Consultation updated" });
  } catch {
    warn $_;
    $self->res->message('Error actualizando consulta');
    $self->rendered(500);
  }
}

sub patient_info($self) {
  my $consultation_id = $self->stash('id');
  try {
    my %patient =
      $self
      ->consultations
      ->find({ id => $consultation_id })
      ->patient
      ->get_columns;
    $self->render(json => { as_utf8(%patient) });
  } catch {
    warn $_;
    $self->res->message('Error obteniendo paciente de consulta');
    $self->rendered(500);
  }
}

sub services($self) {
  my $consultation_id = $self->stash('id');
  try {
    my @services =
      $self
      ->consultations
      ->find({ id => $consultation_id })
      ->services
      ->all;
    my @full_services = map { {  $_->get_columns,
                                  $_->service->get_columns,
                                  $_->service->specialist->get_columns } } @services;
    $self->render(json => \@full_services);
  } catch {
    warn $_;
    $self->res->message('Error obteniendo servicios prestados durante la consulta');
    $self->rendered(500);
  }
}

1

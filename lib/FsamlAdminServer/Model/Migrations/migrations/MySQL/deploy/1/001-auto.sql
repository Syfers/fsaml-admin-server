--
-- Created by SQL::Translator::Producer::MySQL
-- Created on Fri Apr 16 21:13:40 2021
--
;
SET foreign_key_checks=0;
--
-- Table: `areas`
--
CREATE TABLE `areas` (
  `aid` integer(32) NOT NULL,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB;
--
-- Table: `patients`
--
CREATE TABLE `patients` (
  `cid` integer(32) NOT NULL,
  `names` varchar(50) NOT NULL,
  `lastnames` varchar(60) NOT NULL,
  `address` varchar(100) NOT NULL,
  `birthdate` datetime NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB;
--
-- Table: `consultations`
--
CREATE TABLE `consultations` (
  `id` integer(32) NOT NULL auto_increment,
  `patient_id` integer(32) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `status` char(1) NOT NULL,
  `consultation_date` datetime NOT NULL,
  INDEX `consultations_idx_patient_id` (`patient_id`),
  PRIMARY KEY (`id`),
  CONSTRAINT `consultations_fk_patient_id` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
--
-- Table: `specialists`
--
CREATE TABLE `specialists` (
  `cid` integer(32) NOT NULL,
  `names` varchar(50) NOT NULL,
  `lastnames` varchar(65) NOT NULL,
  `area_id` integer(32) NOT NULL,
  INDEX `specialists_idx_area_id` (`area_id`),
  PRIMARY KEY (`cid`),
  CONSTRAINT `specialists_fk_area_id` FOREIGN KEY (`area_id`) REFERENCES `areas` (`aid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
--
-- Table: `schedules`
--
CREATE TABLE `schedules` (
  `day_of_week` tinyint(1) NOT NULL,
  `start_hour` time NOT NULL,
  `duration` time NOT NULL,
  `specialist_id` integer(32) NOT NULL,
  INDEX `schedules_idx_specialist_id` (`specialist_id`),
  PRIMARY KEY (`day_of_week`, `start_hour`, `specialist_id`),
  CONSTRAINT `schedules_fk_specialist_id` FOREIGN KEY (`specialist_id`) REFERENCES `specialists` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
--
-- Table: `services`
--
CREATE TABLE `services` (
  `id` integer(32) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `specialist_id` integer(32) NULL,
  INDEX `services_idx_specialist_id` (`specialist_id`),
  PRIMARY KEY (`id`),
  CONSTRAINT `services_fk_specialist_id` FOREIGN KEY (`specialist_id`) REFERENCES `specialists` (`cid`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB;
--
-- Table: `service_prices`
--
CREATE TABLE `service_prices` (
  `active_date` datetime NOT NULL,
  `inactive_date` datetime NULL,
  `service_id` integer(32) NOT NULL,
  `price` float(32, 0) NOT NULL,
  INDEX `service_prices_idx_service_id` (`service_id`),
  PRIMARY KEY (`active_date`, `service_id`),
  CONSTRAINT `service_prices_fk_service_id` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
--
-- Table: `payments`
--
CREATE TABLE `payments` (
  `id` integer(32) NOT NULL auto_increment,
  `amount` float(32, 0) NOT NULL,
  `exoneration_percentage` float NULL,
  `specialist_contribution` float NULL,
  `exoneration_type` tinyint(1) NULL,
  `consultation_id` integer(32) NOT NULL,
  `exoneration_specialist_id` integer(32) NULL,
  INDEX `payments_idx_consultation_id` (`consultation_id`),
  INDEX `payments_idx_exoneration_specialist_id` (`exoneration_specialist_id`),
  PRIMARY KEY (`id`),
  CONSTRAINT `payments_fk_consultation_id` FOREIGN KEY (`consultation_id`) REFERENCES `consultations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `payments_fk_exoneration_specialist_id` FOREIGN KEY (`exoneration_specialist_id`) REFERENCES `specialists` (`cid`) ON DELETE CASCADE
) ENGINE=InnoDB;
--
-- Table: `payment_statuses`
--
CREATE TABLE `payment_statuses` (
  `active_date` datetime NOT NULL,
  `inactive_date` datetime NULL,
  `payment_id` integer(32) NOT NULL,
  `status` tinyint(1) NOT NULL,
  INDEX `payment_statuses_idx_payment_id` (`payment_id`),
  PRIMARY KEY (`active_date`, `payment_id`),
  CONSTRAINT `payment_statuses_fk_payment_id` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
--
-- Table: `consultation_services`
--
CREATE TABLE `consultation_services` (
  `consultation_id` integer(32) NOT NULL,
  `service_id` integer(32) NOT NULL,
  `active_date` datetime NOT NULL,
  INDEX `consultation_services_idx_consultation_id` (`consultation_id`),
  INDEX `consultation_services_idx_active_date_service_id` (`active_date`, `service_id`),
  PRIMARY KEY (`consultation_id`, `service_id`, `active_date`),
  CONSTRAINT `consultation_services_fk_consultation_id` FOREIGN KEY (`consultation_id`) REFERENCES `consultations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `consultation_services_fk_active_date_service_id` FOREIGN KEY (`active_date`, `service_id`) REFERENCES `service_prices` (`active_date`, `service_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;
SET foreign_key_checks=1;

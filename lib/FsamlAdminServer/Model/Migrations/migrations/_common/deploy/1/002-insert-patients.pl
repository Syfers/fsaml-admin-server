use strict;
use warnings;
use FindBin qw($Bin);
use Parse::CSV;
use DateTime;
use DBIx::Class::Migration::RunScript;

migrate {
  my $patient_rs = shift->schema->resultset('Patient');


  my $file = Parse::CSV->new(
                             file => "$ENV{DATA_SOURCE}/patients.csv",
                             names => 1
                            );

  while (my $record = $file->fetch){
    $record->{birthdate} = DateTime->from_epoch({ epoch => $record->{birthdate} });
    print %{ $record }, '\n';
    $patient_rs->create($record);
  }
}

use strict;
use warnings;
use Parse::CSV;
use DateTime;
use DBIx::Class::Migration::RunScript;

migrate {
  my $schema = shift->schema;
  my $area_rs = $schema->resultset('Area');


  my $area_file = Parse::CSV->new(
                             file => "$ENV{DATA_SOURCE}/areas.csv",
                             names => 1
                            );

  while (my $record = $area_file->fetch){
    print %{ $record }, '\n';
    $area_rs->create($record);
  }

  my $specialist_rs = $schema->resultset('Specialist');


  my $specialist_file = Parse::CSV->new(
                             file => "$ENV{DATA_SOURCE}/specialists.csv",
                             names => 1
                            );

  while (my $record = $specialist_file->fetch){
    print %{ $record }, '\n';
    $specialist_rs->create($record);
  }

  my $service_rs = $schema->resultset('Service');


  my $service_file = Parse::CSV->new(
                             file => "$ENV{DATA_SOURCE}/services.csv",
                             names => 1
                            );

  while (my $record = $service_file->fetch){
    print %{ $record }, '\n';
    $service_rs->create($record);
  }

# my $schedule_rs = $schema->resultset('Schedule');


# my $schedule_file = Parse::CSV->new(
#                            file => "$ENV{DATA_SOURCE}/schedules.csv",
#                            names => 1
#                           );

# while (my $record = $schedule_file->fetch){
#   print %{ $record }, '\n';
#   $record->{duration} = "$record->{duration}:00";
#   $schedule_rs->create($record);
# }

  my $service_price_rs = $schema->resultset('ServicePrice');


  my $service_price_file = Parse::CSV->new(
                             file => "$ENV{DATA_SOURCE}/services_prices.csv",
                             names => 1
                            );

  while (my $record = $service_price_file->fetch){
    print %{ $record }, '\n';
    my @date_components = split '/', $record->{active_date};
    $record->{active_date} = DateTime->new(
                                           year => $date_components[2],
                                           day => $date_components[1],
                                           month => $date_components[0]
                                          );
    $record->{inactive_date} = undef if $record->{inactive_date} eq '';
    if ($record->{inactive_date} ne '')
    {
      my @date_components = split '/', $record->{inactive_date};
      $record->{inactive_date} = DateTime->new(
                                             year => $date_components[2],
                                             day => $date_components[1],
                                             month => $date_components[0]
                                            );
      ($record->{active_date}, $record->{inactive_date}) =
        ($record->{inactive_date}, $record->{active_date})
        if DateTime->compare($record->{inactive_date}, $record->{active_date}) != 1;
    }
    $service_price_rs->create($record);
  }

  my $consultation_rs = $schema->resultset('Consultation')->search;

  my $consultation_file = Parse::CSV->new(
                             file => "$ENV{DATA_SOURCE}/consultations.csv",
                             names => 1
                            );

  while (my $record = $consultation_file->fetch){
    print %{ $record }, '\n';
    my @date_components = split '/', $record->{consultation_date};
    $record->{consultation_date} = DateTime->new(year => $date_components[2],
                                                 day => $date_components[1],
                                                 month => $date_components[0]
                                                );
    $consultation_rs->create($record);
  }

  my @consultation_iter = $consultation_rs->all;

  foreach my $consultation (@consultation_iter) {
    if (int rand 2) {
      my $id = 1 + int rand 19;
      my $service = $service_price_rs->find({
                                             service_id => $id,
                                             inactive_date => { '!=', undef }
                                            });
      if ($service) {
        my %service = $service->get_columns;
        $service->set_consultations($consultation);
      }
    }
  }

  foreach my $consultation (@consultation_iter) {
    my @services =
      map { $_->service_price } $consultation->consultation_services->all;
    my $total_price = 0;
    foreach my $service (@services) {
      $total_price += $service->price;
    }
    my $payments = 1 + int rand 3;
    if ($total_price) {
      for (1..$payments) {
        my $payment =
          $consultation->create_related(payments => {
                                                     amount => $total_price / $payments,
                                                     exoneration_percentage => undef,
                                                     specialist_contribution => undef,
                                                     exoneration_type => undef,
                                                     exoneration_specialist_id => undef
                                                    });
        my $status =
          $schema->resultset('PaymentStatus')->create({
                                                     active_date => DateTime->now,
                                                     inactive_date => undef,
                                                     status => int rand 1,
                                                     payment_id => $payment->id
                                                    });
      }
    }
  }
}

package FsamlAdminServer::Model::Billing::Schema::Result::Patient;
use base DBIx::Class::Core;

__PACKAGE__->table('patients');
__PACKAGE__->add_columns(
                         cid => {
                                 accessor => 'cid',
                                 data_type => 'integer',
                                 size => 32,
                                 is_nullable => 0
                                },
                         names => {
                                   accessor => 'names',
                                   data_type => 'varchar',
                                   size => 50,
                                   is_nullable => 0
                                  },
                         lastnames => {
                                       accessor => 'names',
                                       data_type => 'varchar',
                                       size => 60,
                                       is_nullable => 0
                                     },
                         address => {
                                    accessor => 'names',
                                    data_type => 'varchar',
                                    size => 100,
                                    is_nullable => 0
                                   },
                         birthdate => {
                                       accessor => 'names',
                                       data_type => 'datetime'
                                      });

__PACKAGE__->set_primary_key('cid');

__PACKAGE__->has_many(consultations =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::Consultation',
                      'patient_id');
1

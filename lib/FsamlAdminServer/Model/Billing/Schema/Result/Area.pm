package FsamlAdminServer::Model::Billing::Schema::Result::Area;
use base DBIx::Class::Core;

__PACKAGE__->table('areas');
__PACKAGE__->add_columns(
                          aid => {
                                 accessor => 'aid',
                                 data_type => 'integer',
                                 size => 32,
                                 is_nullable => 0
                                },
                          name => {
                                   accessor => 'name',
                                   data_type => 'varchar',
                                   size => 25,
                                   is_nullable => 0
                                  }
                         );

__PACKAGE__->set_primary_key('aid');

__PACKAGE__->has_many(specialists =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::Specialist',
                      'area_id');


1

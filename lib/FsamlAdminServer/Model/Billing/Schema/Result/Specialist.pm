package FsamlAdminServer::Model::Billing::Schema::Result::Specialist;
use base DBIx::Class::Core;

__PACKAGE__->table('specialists');
__PACKAGE__->add_columns(
                         cid => {
                                 accessor => 'cid',
                                 data_type => 'integer',
                                 size => 32,
                                 is_nullable => 0
                                },
                         names => {
                                  accessor => 'names',
                                  data_type => 'varchar',
                                  size => 50,
                                  is_nullable => 0
                                  },
                         lastnames => {
                                       accessor => 'lastnames',
                                       data_type => 'varchar',
                                       size => 65,
                                       is_nullable => 0
                                      },
                         area_id => {
                                     accesor => 'area_id',
                                     data_type => 'integer',
                                     size => 32,
                                     is_nullable => 0
                                    }
                         );

__PACKAGE__->set_primary_key('cid');

__PACKAGE__->belongs_to(area =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Area',
                        'area_id');

__PACKAGE__->has_many(schedules =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::Schedule',
                      'specialist_id');

__PACKAGE__->has_many(services =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::Service',
                      'specialist_id');

__PACKAGE__->might_have(exonerations =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Payment',
                        'exoneration_specialist_id');
1

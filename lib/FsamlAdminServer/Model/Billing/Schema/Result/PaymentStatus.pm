package FsamlAdminServer::Model::Billing::Schema::Result::PaymentStatus;
use base DBIx::Class::Core;

__PACKAGE__->table('payment_statuses');
__PACKAGE__->add_columns(
                         active_date => {
                                          accessor => 'active_date',
                                          data_type => 'datetime',
                                          is_nullable => 0
                                         },
                         inactive_date => {
                                             accessor => 'inactive_date',
                                             data_type => 'datetime',
                                             is_nullable => 1
                                            },
                         payment_id => {
                                        accessor => 'payment_id',
                                        data_type => 'integer',
                                        size => 32,
                                        is_nullable => 0
                                       },
                         status => {
                                   accessor => 'status',
                                   data_type => 'tinyint',
                                   size => 1,
                                   is_nullable => 0
                                  }
                         );

__PACKAGE__->set_primary_key(qw(active_date payment_id));

__PACKAGE__->belongs_to(payment =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Payment',
                        'payment_id'
                       );

1

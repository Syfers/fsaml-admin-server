package FsamlAdminServer::Model::Billing::Schema::Result::Schedule;
use base DBIx::Class::Core;

__PACKAGE__->table('schedules');
__PACKAGE__->add_columns(
                         day_of_week => {
                                          accessor => 'day_of_week',
                                          data_type => 'tinyint',
                                          size => 1,
                                          is_nullable => 0
                                         },
                         start_hour => {
                                         accessor => 'start_hour',
                                         data_type => 'time',
                                         is_nullable => 0
                                        },
                         duration => {
                                      accessor => 'duration',
                                      data_type => 'time',
                                      is_nullable => 0
                                     },
                         specialist_id => {
                                           accessor => 'specialist_id',
                                           data_type => 'integer',
                                           size => 32,
                                           is_nullable => 0
                                          }
                         );

__PACKAGE__->set_primary_key(qw(day_of_week start_hour specialist_id));

__PACKAGE__->belongs_to(specialist =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Specialist',
                        'specialist_id');

1

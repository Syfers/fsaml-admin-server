package FsamlAdminServer::Model::Billing::Schema::Result::ServicePrice;
use base DBIx::Class::Core;

__PACKAGE__->table('service_prices');
__PACKAGE__->add_columns(
                         active_date => {
                                          accessor => 'active_date',
                                          data_type => 'datetime',
                                          is_nullable => 0
                                         },
                         inactive_date => {
                                             accessor => 'inactive_date',
                                             data_type => 'datetime',
                                             is_nullable => 1
                                            },

                         service_id => {
                                        accessor => 'service_id',
                                        data_type => 'integer',
                                        size => 32,
                                        is_nullable => 0
                                       },
                         price => {
                                   accessor => 'price',
                                   data_type => 'float',
                                   size => 32,
                                   is_nullable => 0
                                  }
                         );

__PACKAGE__->set_primary_key(qw(active_date service_id));

__PACKAGE__->belongs_to(service =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Service',
                        'service_id');

__PACKAGE__->has_many(consultation_services =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::ConsultationService',
                      {
                       'foreign.service_id' => 'self.service_id',
                       'foreign.active_date' => 'self.active_date'
                      });

__PACKAGE__->many_to_many(consultations => 'consultation_services', 'consultation');

1

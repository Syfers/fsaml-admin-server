package FsamlAdminServer::Model::Billing::Schema::Result::Consultation;
use base DBIx::Class::Core;

__PACKAGE__->table('consultations');
__PACKAGE__->add_columns(
                          id => {
                                 accessor => 'id',
                                 data_type => 'integer',
                                 size => 32,
                                 is_nullable => 0,
                                 is_auto_increment => 1
                                },
                          patient_id => {
                                 accessor => 'patient_id',
                                 data_type => 'integer',
                                 size => 32,
                                 is_nullable => 0
                                },
                          reason => {
                                   accessor => 'reason',
                                   data_type => 'varchar',
                                   size => 255,
                                   is_nullable => 0
                                  },
                          status => {
                                   accessor => 'status',
                                   data_type => 'char',
                                   size => 1,
                                   is_nullable => 0
                                  },
                          consultation_date => {
                                   accessor => 'consultation_date',
                                   data_type => 'datetime',
                                   is_nullable => 0
                                  }
                         );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(patient =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Patient',
                        'patient_id');

__PACKAGE__->has_many(consultation_services =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::ConsultationService',
                      'consultation_id');

__PACKAGE__->many_to_many(services => 'consultation_services', 'service_price');

__PACKAGE__->has_many(payments =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::Payment',
                      'consultation_id');
1

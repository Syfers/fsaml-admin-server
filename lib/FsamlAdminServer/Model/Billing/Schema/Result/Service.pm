package FsamlAdminServer::Model::Billing::Schema::Result::Service;
use base DBIx::Class::Core;

__PACKAGE__->table('services');
__PACKAGE__->add_columns(
                          id => {
                                 accessor => 'cid',
                                 data_type => 'integer',
                                 size => 32,
                                 is_auto_increment => 1,
                                 is_nullable => 0
                                },
                          name => {
                                   accessor => 'names',
                                   data_type => 'varchar',
                                   size => 50,
                                   is_nullable => 0
                                  },
                          specialist_id => {
                                   accessor => 'specialist_id',
                                   data_type => 'integer',
                                   size => 32,
                                   is_nullable => 1
                                  },
                         );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(specialist =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Specialist',
                        'specialist_id',
                        {
                         join_type => 'left',
                         is_foreign_key_constraint => 1,
                         on_delete => 'set null'
                        });

__PACKAGE__->has_many(prices =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::ServicePrice',
                      'service_id');
1

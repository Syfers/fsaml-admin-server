package FsamlAdminServer::Model::Billing::Schema::Result::ConsultationService;
use base DBIx::Class::Core;

__PACKAGE__->table('consultation_services');
__PACKAGE__->add_columns(
                         consultation_id => {
                                             accessor => 'consultation_id',
                                             data_type => 'integer',
                                             size => 32,
                                             is_nullable => 0
                                            },
                         service_id => {
                                              accessor => 'service_id',
                                              data_type => 'integer',
                                              size => 32,
                                              is_nullable => 0
                                             },
                         active_date => {
                                         accessor => 'active_date',
                                         data_type => 'datetime',
                                         is_nullable => 0
                                        }
                         );

__PACKAGE__->set_primary_key(qw(consultation_id service_id active_date));

__PACKAGE__->belongs_to(consultation =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Consultation',
                        'consultation_id');

__PACKAGE__->belongs_to(service_price =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::ServicePrice',
                        {
                         'foreign.service_id' => 'self.service_id',
                         'foreign.active_date' => 'self.active_date'
                        });

1

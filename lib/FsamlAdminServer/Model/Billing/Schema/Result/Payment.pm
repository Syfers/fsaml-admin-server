package FsamlAdminServer::Model::Billing::Schema::Result::Payment;
use base DBIx::Class::Core;

__PACKAGE__->table('payments');
__PACKAGE__->add_columns(
                         id => {
                                accessor => 'id',
                                data_type => 'integer',
                                size => 32,
                                is_nullable => 0,
                                is_auto_increment => 1
                               },
                         amount => {
                                    accessor => 'amount',
                                    data_type => 'float',
                                    size => 32,
                                    is_nullable => 0
                                   },
                         exoneration_percentage => {
                                                    accessor => 'exoneration_percentage',
                                                    data_type => 'float',
                                                    is_nullable => 1
                                                   },
                         specialist_contribution => {
                                                     accessor => 'specialist_contribution',
                                                     data_type => 'float',
                                                     is_nullable => 1
                                                   },
                         exoneration_type => {
                                              accessor => 'exoneration_type',
                                              data_type => 'tinyint',
                                              size => 1,
                                              is_nullable => 1
                                             },
                         consultation_id => {
                                             accessor => 'consultation_id',
                                             data_type => 'integer',
                                             size => 32,
                                             is_nullable => 0
                                            },
                         exoneration_specialist_id => {
                                                    accessor => 'exoneration_specialist_id',
                                                    data_type => 'integer',
                                                    size => 32,
                                                    is_nullable => 1
                                                   }
                         );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(exoneration_specialist =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Specialist',
                        'exoneration_specialist_id',
                        { join_type => 'left' });

__PACKAGE__->belongs_to(consultation =>
                        'FsamlAdminServer::Model::Billing::Schema::Result::Consultation',
                        'consultation_id');

__PACKAGE__->has_many(statuses =>
                      'FsamlAdminServer::Model::Billing::Schema::Result::PaymentStatus',
                      'payment_id');

1

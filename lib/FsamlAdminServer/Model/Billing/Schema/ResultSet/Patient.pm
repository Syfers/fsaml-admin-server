package FsamlAdminServer::Model::Billing::Schema::ResultSet::Patient;
use Mojo::JSON qw(encode_json);
use Mojo::Base 'DBIx::Class::ResultSet', -role;
with 'FsamlAdminServer::Role::StreamResultSetRole';

sub is_like {
  my $self = shift;
  my $query = shift;
  my $data = shift;
  my $column = shift;
  my $cb = sub {
    eval q(
use FsamlAdminServer::Role::StreamResultSetRole;
FsamlAdminServer::Role::StreamResultSetRole::is_like(@_)
);
  };
  return $cb->($self, $query, $data, $column) unless $column eq 'birthdate';
  return $self->is_of_age($query, $data, $column);
}

sub parsing_rules {
  return {
          cid => [ 'cid' ],
          fullname => [ 'names', 'lastnames' ],
          birthdate => [ 'birthdate' ],
          address => [ 'address' ]
         };
}

sub is_of_age {
  my $self = shift;
  my $query = shift;
  my $data = shift;
  my $column = shift;

  return push @$query,
    {
     $column =>
     {
      -between =>
      [
       \(["DATE_SUB(NOW(), INTERVAL ? year)", $data + 1]),
       \(["DATE_SUB(DATE_SUB(NOW(), INTERVAL ? year), INTERVAL 1 day)", $data])
      ]
     }
    } if $data + 0 != 0 || $data =~ m(^0?(0*)?0$);
  return push @$query, \(["false"]);
}

sub prepare_operation_query {
  my $self = shift;
  my $query = shift;
  my $type = lc shift;

  return $self->search($query, { rows => 10, order_by => { -asc => 'cid' } })
    unless $type eq 'all' || $type eq 'fullname';
  return $self->search([ -or => $query ], { rows => 10, order_by => { -asc => 'cid' } });
}

1

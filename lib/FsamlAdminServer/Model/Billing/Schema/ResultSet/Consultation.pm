package FsamlAdminServer::Model::Billing::Schema::ResultSet::Consultation;
use Mojo::JSON qw(encode_json);
use Role::Tiny::With;
use Mojo::Base 'DBIx::Class::ResultSet';
with 'FsamlAdminServer::Role::StreamResultSetRole';

sub parsing_rules {
  return {
          status => [ 'status' ],
          cid => [ 'cid' ],
          consultationdate => [ 'consultation_date' ],
          reason => [ 'reason' ]
         };
}

sub is_like {
  my $self = shift;
  my $query = shift;
  my $data = shift;
  my $column = shift;
  my $cb = sub {
    eval q(require FsamlAdminServer::Role::StreamResultSetRole;
FsamlAdminServer::Role::StreamResultSetRole::is_like(@_));
  };

  $cb->($self, $query, $data, $column) if $column eq 'cid' || $column eq 'reason';
  $self->is_status_like($query, $data, $column) if $column eq 'status';
  $self->is_date_before($query, $data, $column) if $column eq 'consultation_date';
}

sub is_status_like {
  my ($_, $query, $data) = @_;

  push @$query, \([ q(
CASE status WHEN 1 THEN 'Cancelada'
WHEN 2 THEN 'En Proceso'
WHEN 3 THEN 'Aceptada'
ELSE FALSE
END LIKE ?
), "%$data%" ]);
}

sub is_date_before {
  my ($_, $query, $data) = @_;
  push @$query, \([q(UNIX_TIMESTAMP(consultation_date) <= ?), $data]);
}

sub prepare_operation_query {
  my $self = shift;
  my $query = shift;
  my $type = lc(shift);

  return $self->search($query, { rows => 10, order_by => { -asc => 'cid' } })
    unless $type eq 'all' || $type eq 'consultationdate';
  return $self->search($query, { rows => 10, order_by => { -desc => 'consultation_date' } })
    if $type eq 'consultationdate';
  return $self->search([ -or => $query ], { rows => 10, order_by => { -asc => 'cid' } });
}

1

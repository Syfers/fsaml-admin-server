package FsamlAdminServer::Role::StreamResultSetRole;
use Mojo::JSON qw(encode_json);
use Mojo::Base -role, -signatures;

requires 'parsing_rules', 'prepare_operation_query';

sub is_like {
  my $self = shift;
  my $query = shift;
  my $data = shift;
  my $column = shift;

  $self->result_source
    ->schema
    ->storage
    ->dbh_do(sub {
               my ($storage, $dbh, @args) = @_;
               $dbh->do("SET NAMES 'utf8';");
    });
  push @$query, { $column => { like => "%$data%" } };
}


sub operate {
  my $self = shift;
  my $query_params = shift;
  my $PARSE = $self->parsing_rules;
  my @all_rules = map { @$_ } values %$PARSE;
  $PARSE->{all} = \@all_rules;
  my $query = [] ;

  foreach my $column (@{ $PARSE->{lc $query_params->{type}} }) {
    my $operation = $query_params->{operation};
    $self->$operation($query, $query_params->{search}, $column);
  }

  return
    $self
    ->prepare_operation_query($query, $query_params->{type})
    ->page($query_params->{page} || 1)
}

1

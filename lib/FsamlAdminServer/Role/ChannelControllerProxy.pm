package FsamlAdminServer::Role::ChannelControllerProxy;
use Mojo::Base -role, -signatures;

requires 'prefix', 'register', 'controller';

sub controller_connected($self) {
  return $self->prefix(ref $self->controller)
}

sub current($self) {
  my $controller = $self->controller;
  my $topic = shift || ref $controller;
  my $id = $self->controller->req->request_id;
  my $members = $self->prefix("$topic:$id")->members;
  return $members->[0] if $members && $members->[0];
  my $class_factory = $self->container->service_class;
  $self->register(my $service = $class_factory->($controller));
  $controller->on(finish => sub {
                    $controller->app->log->debug(
                                                 sprintf "Unregistering connection under: %s",
                                                 $service->id
                                                );
                    $self->unregister($service)
                  });
  return $service;
}

sub all($self) {
  return $self->container->prefix("*")
}

1

package FsamlAdminServer::Role::StreamRole;
use utf8;
use utf8::all;
use Mojo::JSON qw(decode_json);
use Encode;
use Mojo::Base -role, -signatures;

requires 'stream_rs';

sub stream_handshake($self) {
  $self->inactivity_timeout(0);
  my $worker = $self->workers->current;

  $self->on(message => sub($c, $msg) {
              my $query_params = decode_json encode_utf8($msg);
              my $results =
                $c
                ->stream_rs
                ->operate($query_params);
              $c->send({
                        json =>
                        {
                         total_pages =>
                         $results->pager->total_entries ?
                         $results->pager->last_page : 0,
                         query_id => $query_params->{query_id}
                        }
                       });
              $worker
                ->cancel
                ->send(timeout => 0,
                       cb =>
                       sub ($worker, $query_params) {
                         my $package = $worker->query->next;
                         if ($package && $worker->tx) {
                           my %package_data = $package->get_columns;
                           $package_data{query_id} = $query_params->{query_id};
                           foreach my $key (keys %package_data) {
                             $package_data{$key} = decode_utf8($package_data{$key});
                           }
                           $worker->send(json => \%package_data);
                         } else {
                           $worker->cancel;
                         }
                       },
                       query => $results,
                       params => [ $query_params ]);
              $c->continue;
            });
}

sub restream($self) {
  $self
    ->workers
    ->controller_connected
    ->refresh;
}

1
